// Angular App for Panel
angular.module('csApp', [])

.config(function(){

})

.run(function($rootScope) {

})

.controller('MainCtrl', ['$scope', function($scope) {

  var init = function() {
    var mime = 'text/x-mysql';
    initCodeMirror(mime);
  };

  var initCodeMirror = function(mime) {
    // get mime type
    window.editor = CodeMirror.fromTextArea(document.getElementById('code'), {
      mode: mime,
      indentWithTabs: true,
      smartIndent: true,
      lineNumbers: true,
      matchBrackets: true,
      autofocus: true,
      extraKeys: {
        "Ctrl-Space": "autocomplete"
      },
      hintOptions: {
        tables: {
          users: {
            name: null,
            score: null,
            birthDate: null
          },
          countries: {
            name: null,
            population: null,
            size: null
          }
        }
      }
    });
  }

  init();

}])
